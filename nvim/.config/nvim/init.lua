--
-- General nvim options
--

-- Enable line numbers 
vim.opt.number = true
-- Enable relative line numbers
vim.opt.relativenumber = true
-- Enable case-insensitive lowercase search
vim.opt.ignorecase = true
-- Enable smart-case search
vim.opt.smartcase = true

--
-- Keybindings
--

-- Keybinding to open netrw
vim.api.nvim_set_keymap("n", "<C-e>", ":Lexplore<CR>", {noremap = true, silent = true})
